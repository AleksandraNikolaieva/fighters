import { controls } from '../../constants/controls';

const fightersBlocks = new Set();
const fightersActiveCriticalControls = new Map()

export async function fight(firstFighter, secondFighter) {
    return new Promise((resolve) => {
        firstFighter.healthIndicator = document.getElementById('left-fighter-indicator');
        firstFighter.currentHealth = firstFighter.health;
        
        secondFighter.healthIndicator = document.getElementById('right-fighter-indicator');
        secondFighter.currentHealth = secondFighter.health;
        
        const firstFighterId = firstFighter._id;
        const secondFighterId = secondFighter._id;
    
        fightersActiveCriticalControls.set(firstFighterId, new Set());
        fightersActiveCriticalControls.set(secondFighterId, new Set());

        accumulateCriticalPower(firstFighter);
        accumulateCriticalPower(secondFighter);

        addFightersActionStartListeners(firstFighterId, secondFighterId);
        addFightersActionEndListeners(firstFighter, secondFighter, resolve);
    });
}

export function getDamage(attacker, defender) {
    const diff = getHitPower(attacker) - getBlockPower(defender);
    return diff > 0 ? diff : 0;
}

export function getHitPower(fighter) {
    const criticalHitChance = getRandom(1, 2);
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = getRandom(1, 2);
    return fighter.defense * dodgeChance;
}

function getCriticalHitPower(fighter) {
    return fighter.attack * 2;
}

function getRandom(from, to) {
    return from + Math.random() * (to - from);
}

function getNewHealthPersentage(initialHealth, currentHealth) {
    return currentHealth > 0 ? (currentHealth / initialHealth) * 100 + '%' : 0;
}

function accumulateCriticalPower(fighter) {
    setTimeout(() => {
        fighter.hasCriticalHitPower = true;
    }, 10000);
}

function getHitDamage(attacker, defender, hitType) {
    let damage = 0;
    const criticalHitCombinationLength = 3;

    if (hitType === 'common' && !fightersBlocks.has(attacker._id)) {
        damage = fightersBlocks.has(defender._id) ? getDamage(attacker, defender) : getHitPower(attacker);
    } else if (hitType === 'critical' && attacker.hasCriticalHitPower && fightersActiveCriticalControls.get(attacker._id).size === criticalHitCombinationLength) {
        damage = getCriticalHitPower(attacker);

        attacker.hasCriticalHitPower = false;
        accumulateCriticalPower(attacker);

        fightersActiveCriticalControls.get(attacker._id).delete(event.code);
    }

    return damage;
}

function addFightersActionStartListeners(firstFighterId, secondFighterId) {
    window.addEventListener('keydown', (event) => {
        if (event.code === controls.PlayerOneBlock) {
            fightersBlocks.add(firstFighterId);
        } else if (event.code === controls.PlayerTwoBlock) {
            fightersBlocks.add(secondFighterId);
        } else if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
            fightersActiveCriticalControls.get(firstFighterId).add(event.code);
        } else if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
            fightersActiveCriticalControls.get(secondFighterId).add(event.code);
        }
    })
}

function addFightersActionEndListeners(firstFighter, secondFighter, onFinish) {

    window.addEventListener('keyup', (event) => {

        if (event.code === controls.PlayerOneBlock) {
            fightersBlocks.delete(firstFighter._id)
        } else if (event.code === controls.PlayerTwoBlock) {
            fightersBlocks.delete(secondFighter._id);
        }

        const { attacker, defender, hitType } = getFightersRolesAndHitType(firstFighter, secondFighter)

        if (attacker && defender) {
            const damage = getHitDamage(attacker, defender, hitType);

            if (damage) {
                defender.currentHealth -= damage;
                defender.healthIndicator.style.width = getNewHealthPersentage(defender.health, defender.currentHealth);

                if (defender.currentHealth <= 0) {
                    onFinish(attacker);
                }
            }
        }
    })
}

function getFightersRolesAndHitType(firstFighter, secondFighter) {
    let defender;
    let attacker;
    let hitType;

    const isPlayerOneCommonHit = event.code === controls.PlayerOneAttack;
    const isPlayerTwoCommonHit = event.code === controls.PlayerTwoAttack;
    const isPlayerOneCriticalHit = controls.PlayerOneCriticalHitCombination.includes(event.code);
    const isPlayerTwoCriticalHit = controls.PlayerTwoCriticalHitCombination.includes(event.code);

    if (isPlayerOneCommonHit || isPlayerOneCriticalHit) {
        attacker = firstFighter;
        defender = secondFighter;
    } else if (isPlayerTwoCommonHit || isPlayerTwoCriticalHit) {
        attacker = secondFighter;
        defender = firstFighter;
    }

    if (isPlayerOneCommonHit || isPlayerTwoCommonHit) {
        hitType = 'common'
    } else if (isPlayerOneCriticalHit || isPlayerTwoCriticalHit) {
        hitType = 'critical'
    }

    return {defender, attacker, hitType};
}
