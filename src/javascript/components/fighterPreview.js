import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    if (fighter) {
        const { name, health, attack, defense } = fighter;
        const fighterName = createElement({ tagName: 'h2', className: 'fighter-preview-name' });
        fighterName.innerText = name;
        const fighterImage = createFighterImage(fighter);
        const fighterInfo = createElement({ tagName: 'div', className: 'fighter-preview-info' });
        fighterInfo.innerText = `Health: ${health}\nAttack: ${attack}\nDefense: ${defense}`;
        fighterElement.append(fighterName, fighterImage, fighterInfo);
    }

    return fighterElement;
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}
