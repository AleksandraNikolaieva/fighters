import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';
import { removeArena } from '../arena';
import App from '../../app';

export function showWinnerModal(fighter) {
    const winnerImage = createFighterImage(fighter);
    const onClose = () => { removeArena(); new App()};
    showModal({title: `${fighter.name} won!`, bodyElement: winnerImage, onClose});
}
